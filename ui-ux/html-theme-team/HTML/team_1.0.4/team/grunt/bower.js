module.exports = function (grunt) {

    return {
        extract: {
            options: {
                packageSpecific: {
                    'jquery': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/jquery.js'
                        ]
                    },
                    'jquery-ui': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'jquery-ui.js',
                            'themes/base/slider.css'
                        ]
                    },
                    'font-awesome': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'css/font-awesome.css',
                            'fonts/*'
                        ]
                    },
                    'bootstrap': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/js/bootstrap.js',
                            'dist/css/bootstrap.css',
                            'dist/fonts/*'
                        ]
                    },
                    'animate.css': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'animate.css'
                        ]
                    },
                    'jcarousel': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/jquery.jcarousel.js',
                            'examples/connected-carousels/jcarousel.connected-carousels.js'
                        ]
                    },
                    'jquery-sticky': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'jquery.sticky.js'
                        ]
                    },
                    'owl.carousel': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/owl.carousel.js',
                            'dist/assets/owl.carousel.css',
                            'dist/assets/owl.theme.default.css'
                        ]
                    },
                    'progressbar.js': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/progressbar.js'
                        ]
                    },
                    'isotope': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/isotope.pkgd.js'
                        ]
                    },
                    'jquery-bracket': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/jquery.bracket.min.js',
                            'dist/jquery.bracket.min.css'
                        ]
                    },
                    'fancyselect': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'fancySelect.js',
                            'fancySelect.css'
                        ]
                    },
                    'chartist': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/chartist.js',
                            'dist/chartist.css'
                        ]
                    },
                    'checkbox.css': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/css/checkbox.min.css'
                        ]
                    },
                    'imagesloaded': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'imagesloaded.pkgd.js'
                        ]
                    },
                    'chart.js': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/Chart.js'
                        ]
                    },
                    'chartist-plugin-legend': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'chartist-plugin-legend.js'
                        ]

                    },
                    'chartist-plugin-threshold': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/chartist-plugin-threshold.js'
                        ]

                    },

                    'chartist-plugin-pointlabels': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'dist/chartist-plugin-pointlabels.js'
                        ]

                    },
                    'xobotyi': {
                        keepExpandedHierarchy: false,
                        stripGlobBase: true,
                        files: [
                            'jquery.viewport.min.js'
                        ]

                    }
                   
                   
                   
                   
                   

                }
            },
            dest: 'js/library',
            js_dest: 'js/library',
            css_dest: 'css/library',
            fonts_dest: 'fonts'
        }
    };

};