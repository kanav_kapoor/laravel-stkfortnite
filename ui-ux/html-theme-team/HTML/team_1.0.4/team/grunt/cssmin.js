module.exports = function (grunt) {
    
    var files = {};
    
    files['css-min/' + grunt.option('scheme') + '.min.css'] = [
        
        /**
         * This is sequential including for creating /min-css/*.min.css files.
         * You can add other libraries if you need!
         */
        'css/library/bootstrap.css',
        'css/library/owl.carousel.css',
        'css/library/owl.theme.default',
        'css/library/jquery.bracket.min.css',
        'css/library/chartist.css',
        'css/library/font-awesome.css',
        'css/library/fancySelect.css',
        'css/library/checkbox.min.css',
        'css/library/slider.css',
        'css/library/animate.css',
        'css/' +  grunt.option('scheme') + '.css'
    ];
    
    return {
        css: {
            files: files
        }
    };
    
};