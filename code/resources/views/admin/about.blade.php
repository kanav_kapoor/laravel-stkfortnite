@extends('admin.layouts.app')

@push('css')
    {!! Html::style('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css') !!}
@endpush

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">About Us</span>
                    </div>
                   
                </div>
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible" role="alert" style="text-transform: capitalize;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('success') }}
                    </div>
                @endif
                <div class="portlet-body">
                    {!! Form::open(['route' => ['admin.about.update', $about->id ], 'id' => 'form_sample_3', 'method' => 'post', 'files' => true]) !!}

                        <div class="form-body">
                           
                            <div class="form-group form-md-line-input form-md-floating-label">
                                {!! Form::label('content', 'Content') !!}
                                {!! Form::textarea('content', $about->content, ['class' => 'form-control', 'id' => 'content']) !!}
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::button('Update Changes', ['class' => 'btn dark', 'type' => 'submit']) !!}
                                    {!! Form::button('Reset', ['class' => 'btn default', 'type' => 'reset', 'id' => 'reset']) !!}
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    <!-- END CONTENT-->
@endsection

@push('js')
    {!! Html::script('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js') !!}

    <script>
        $(function(){

            $('#content').summernote({
                placeholder: 'Write some new content here........',
                height: 200,
                minHeight: 100,
                maxHeight: 800

            });
            
            $('#reset').on("click", function(e) {
                $('#content').summernote('reset');
            });

        });
        
    </script>
@endpush