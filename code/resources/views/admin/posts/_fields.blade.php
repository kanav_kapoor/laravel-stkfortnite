<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label">
        {!! Form::label('heading', 'Heading') !!}
        {!! Form::textarea('heading', null, ['class' => 'form-control', 'id' => 'heading']) !!}
    </div>
   
    <div class="form-group form-md-line-input form-md-floating-label">
        {!! Form::label('content', 'Content') !!}
        {!! Form::textarea('content', null, ['class' => 'form-control', 'id' => 'content']) !!}
    </div>
    
</div>