@extends('admin.layouts.app')

@push('css')
    {!! Html::style('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css') !!}
@endpush

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">insert {{ $post_name }} </span>
                    </div>
                   
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['admin.posts.store', 'post_identifier' => $post_name ], 'id' => 'form_sample_3', 'method' => 'post', 'files' => true]) !!}
                        @include('admin.posts._fields')
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::button('Insert', ['class' => 'btn dark', 'type' => 'submit']) !!}
                                    {!! Form::button('Reset', ['class' => 'btn default', 'type' => 'reset', 'id' => 'reset']) !!}
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    <!-- END CONTENT-->
@endsection

@push('js')
    {!! Html::script('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js') !!}

    <script>
        $(function(){
            $('#heading').summernote({
                placeholder: 'Got some new title, type here......',
                height: 100,
                minHeight: 50,
                maxHeight: 200, 
                focus: true

            });

            $('#content').summernote({
                placeholder: 'Write some new content here........',
                height: 200,
                minHeight: 100,
                maxHeight: 800

            });
            
            $('#reset').on("click", function(e) {
                $('#heading').summernote('reset');
                $('#content').summernote('reset');
            });

        });
        
    </script>
@endpush