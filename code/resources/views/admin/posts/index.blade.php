@extends('admin.layouts.app')

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::style('metronic/global/plugins/datatables/datatables.min.css') !!}
    {!! Html::style('metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') !!}
    <!-- END PAGE LEVEL PLUGINS -->
@endpush
        
@section('content')
    <!-- BEGIN CONTENT -->
    @php 
        $post_name = str_replace('admin/','', request()->path()) 
    @endphp

        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admin.home') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Posts</span>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li style="text-transform: capitalize;">
                   {{ $post_name }}
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->

         @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert" style="text-transform: capitalize;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session('success') }}
            </div>
        @endif

        <div class="m-heading-1 border-green m-bordered">
            <h3>Displaying currently active Posts</h3>
            {{-- <p> You can manipulate data according to your requirements.</p> --}}
            <p> To add more {{ $post_name }}, please click this button
                <a class="btn red btn-outline uppercase" href="{{ route('admin.posts.create',[ $post_name ]) }}"><span class="glyphicon glyphicon-plus"></span> add {{ $post_name }}</a>
            </p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class=" icon-layers font-red"></i>
                            
                            <span class="caption-subject font-red  sbold uppercase">{{ $post_name }}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1" style="text-transform: capitalize;">
                            <thead>
                                <tr class="">
                                    <th class="col-md-1"> # </th>
                                    <th class="col-md-2"> heading </th>
                                    <th class="col-md-3"> content </th>
                                    <th class="col-md-1"> likes </th>
                                    <th class="col-md-2"> cover photo </th>
                                    <th class="col-md-3"> action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td> {{ $loop->iteration }} </td>
                                        <td> {{ strip_tags($post->heading) }} </td>
                                        <td> @php
                                                $content = strip_tags($post->content);
                                                $final_content = str_limit($content, 50);
                                             @endphp
                                            {{ $final_content }}
                                        </td>
                                        <td> {{ $post->likes_count }} </td>
                                        <td> <img width="100" src="{{ $post->cover_photo_url }}" alt=""> </td>
                                        <td>
                                            <a class="btn dark btn-outline btn-sm" href="{{ route('admin.posts.edit', [$post_name, $post->id]) }}"><span class="glyphicon glyphicon-cog"></span> Edit</a>

                                            <a class="btn red btn-outline btn-sm" href="{{ $post_name }}/{{ $post->id }}/delete" data-method="delete" data-confirm="Are you sure?">
                                                <span class="glyphicon glyphicon-trash "></span> Delete
                                            </a>
                                            
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

    <!-- END CONTENT -->

@endsection


                      
                     
            
            
@push('js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::script('metronic/global/scripts/datatable.js') !!}
    {!! Html::script('metronic/global/plugins/datatables/datatables.min.js') !!}
    {!! Html::script('metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {!! Html::script('metronic/pages/scripts/table-datatables-fixedheader.min.js') !!}
    <!-- END PAGE LEVEL SCRIPTS -->


    <script>
        window.csrfToken = '<?php echo csrf_token(); ?>';
    </script>

    {!! Html::script('js/deleteHandler.js') !!}
@endpush