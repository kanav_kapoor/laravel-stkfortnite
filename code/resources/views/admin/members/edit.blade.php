@extends('admin.layouts.app')

@section('content')
    <h3 class="page-title"> {{ $member->first_name.' '.$member->last_name }}
        <small>( <b>{{ $member->rank->name }}</b> )</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{ route('admin.home') }}">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>Members</span>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span>{{ $member->first_name.' '.$member->last_name }}</span>
            </li>
        </ul>
        
    </div>
    <!-- END PAGE HEADER-->
    <div class="m-heading-1 border-red m-bordered">
        <h3>Important Information</h3>
        <p> All the fields below are required except <b class="text-danger">Password</b> which you can update if required.
        </p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Basic Info</span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                     {!! Form::open(['route' => ['admin.members.update', $member->id], 'id' => 'form_sample_1', 'method' => 'PATCH', 'class' => 'form-horizontal']) !!}
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            <div class="alert alert-success display-hide">
                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Rank
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6">
                                    <select class="form-control" name="rank">
                                        @foreach ($rank as $rank)
                                            <option value="{{ $rank->id }}">{{ $rank->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">First Name
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6">
                                    {!! Form::text('name', $member->first_name, ['class' => 'form-control']) !!}
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">first name required</span>
                                </div>
                            </div>

                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Last Name
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6">
                                    {!! Form::text('name', $member->last_name, ['class' => 'form-control']) !!}
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">last name required</span>
                                </div>
                            </div>

                             <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Email
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        {!! Form::email('email', $member->email, ['class' => 'form-control', 'placeholder' => 'Enter your Email']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Country
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6">
                                    {!! Form::select('country_code', $countries, $member->country_code, ['class' => 'form-control', 'placeholder' => '--Select Your Country--', 'required' => 'required']) !!}
                                    
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Password</label>
                                <div class="col-md-6">
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Bio
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6">
                                    <div class="input-icon">
                                        {!! Form::textarea('bio', $member->bio, ['class' => 'form-control']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            
                           
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Epic Username
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        {!! Form::text('epic-username', $member->epic_username, ['class' => 'form-control']) !!}
                                        <span class="input-group-addon">
                                        </span>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>   

                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Epic Platform
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        {!! Form::text('epic-platform', $member->epic_platform, ['class' => 'form-control']) !!}

                                        <span class="input-group-addon">
                                        </span>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Featured
                                </label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        {!! Form::radio('featured', 1, false, ['class' => 'md-radiobtn', 'id' => 'checkbox1_8', 'checked' => ($member->is_featured == null) ? true : false]) !!}

                                        <label for="checkbox1_8">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Yes 
                                        </label>
                                    </div>
                                    <div class="md-radio">
                                        {!! Form::radio('featured', null, false, ['class' => 'md-radiobtn', 'id' => 'checkbox1_9', 'checked' => ($member->is_featured == null) ? true : false]) !!}
                                        <label for="checkbox1_9">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> No 
                                        </label>
                                    </div>
                                    
                                </div>
                            </div> 
                           
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!! Form::button('Update', ['class' => 'btn green', 'type' => 'submit']) !!}
                                    {!! Form::button('Reset', ['class' => 'btn default', 'type' => 'reset']) !!}
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@endsection

@push('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::script('metronic/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {!! Html::script('metronic/pages/scripts/form-validation-md.min.js') !!}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush