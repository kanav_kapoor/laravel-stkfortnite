<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {!! Html::style('https://use.fontawesome.com/releases/v5.3.1/css/all.css') !!}
    {!! Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') !!}
    {!! Html::script('theme/plugins/jquery/jquery-3.2.1.min.js') !!}
    {!! Html::style('metronic/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}
    {!! Html::style('metronic/global/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}
    <!-- END GLOBAL MANDATORY STYLES -->


    <!-- BEGIN THEME GLOBAL STYLES -->
    {!! Html::style('metronic/global/css/components-rounded.min.css') !!}
    {!! Html::style('metronic/global/css/plugins.min.css') !!}
    <!-- END THEME GLOBAL STYLES -->

    <!-- CSS TO BE OVER-RIDE WITH LOGIN CSS -->
    @section('portalCss')
        <!-- BEGIN THEME LAYOUT STYLES -->
        {!! Html::style('metronic/layouts/layout2/css/layout.min.css') !!}
        {!! Html::style('metronic/layouts/layout2/css/themes/blue.min.css') !!}
        {!! Html::style('metronic/layouts/layout2/css/custom.min.css') !!}        
        <!-- END THEME LAYOUT STYLES -->
    @show
        
    @stack('css')
</head>

@section('body')
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
@show

@auth
    <!-- header -->
    @include('admin.layouts.partials._header')
    <!-- /header -->

    <!-- sidebar -->
    @include('admin.layouts.partials._sidebar')
    <!-- /sidebar -->

     <div class="page-content-wrapper">
        <div class="page-content">
            <!-- main -->
            @yield('content')
            <!-- /main -->
        </div>
    </div>

    <!-- footer -->
    @include('admin.layouts.partials._footer')
    <!-- /footer -->
@endauth


@guest
    @yield('login')
@endguest


<!-- BEGIN CORE PLUGINS -->
{!! Html::script('metronic/global/plugins/jquery.min.js') !!}
{!! Html::script('metronic/global/plugins/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('metronic/global/plugins/js.cookie.min.js') !!}
{!! Html::script('metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}
{!! Html::script('metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
{!! Html::script('metronic/global/plugins/jquery.blockui.min.js') !!}
{!! Html::script('metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
{!! Html::script('metronic/global/scripts/app.min.js') !!}
<!-- END THEME GLOBAL SCRIPTS -->
        
@section('pageLevelScripts')
     <!-- BEGIN PAGE LEVEL PLUGINS -->
     {!! Html::script('metronic/global/plugins/counterup/jquery.waypoints.min.js') !!}
     {!! Html::script('metronic/global/plugins/counterup/jquery.counterup.min.js') !!}
    <!-- END PAGE LEVEL PLUGINS -->


    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    {!! Html::script('metronic/layouts/layout2/scripts/layout.min.js') !!}
    {{-- {!! Html::script('metronic/layouts/layout2/scripts/demo.min.js') !!} --}}
    {!! Html::script('metronic/layouts/global/scripts/quick-sidebar.min.js') !!}
    <!-- END THEME LAYOUT SCRIPTS -->

    {!! Html::script('plugins/jquery-validator/jquery.validate.js') !!}
@show


<script>
    $(function(){ 
         // $('form').each(function(){ $(this).validate(); });
        // $('a, [data-toggle="tooltip"]').tooltip(); 
    });
</script>

@stack('js')
</body>
</html>