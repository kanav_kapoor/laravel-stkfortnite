
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- END SIDEBAR -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start">
                    <a href="{{ route('admin.home') }}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Home</span>
                        {{-- <span class="selected"></span>
                        <span class="arrow open"></span> --}}
                    </a>
                </li>
               
                <li class="nav-item  ">
                    <a href="{{ route('admin.about') }}" class="nav-link nav-toggle">
                        <i class="icon-social-dribbble"></i>
                        <span class="title">About</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{ route('admin.members.index') }}" class="nav-link nav-toggle">
                        <i class="icon-diamond"></i>
                        <span class="title">Members</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                 <li class="nav-item  ">
                    <a href="{{ route('admin.posts.index', 'blogs') }}" class="nav-link ">
                        <i class="icon-pencil"></i>
                        <span class="title">Blogs</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{ route('admin.posts.index', 'news') }}" class="nav-link ">
                        <i class="far fa-newspaper"></i>
                        <span class="title">News</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{ route('admin.events.index') }}" class="nav-link nav-toggle">
                        <i class="icon-calendar"></i>
                        <span class="title">Calendar</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-basket"></i>
                        <span class="title">eCommerce</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="#" class="nav-link ">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="e#" class="nav-link ">
                                <i class="icon-basket"></i>
                                <span class="title">Orders</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="#" class="nav-link ">
                                <i class="icon-tag"></i>
                                <span class="title">Order View</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="#" class="nav-link ">
                                <i class="icon-graph"></i>
                                <span class="title">Products</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="#" class="nav-link ">
                                <i class="icon-graph"></i>
                                <span class="title">Product Edit</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->