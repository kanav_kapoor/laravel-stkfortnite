@extends('admin.layouts.app')

@push('css')
    {!! Html::style('http://www.jqueryscript.net/css/jquerysctipttop.css') !!}
    {!! Html::style('dateTimePicker/DateTimePicker.css') !!}
@endpush

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">insert event </span>
                    </div>
                   
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['admin.events.store'], 'id' => 'form_sample_3', 'method' => 'post']) !!}
                        @include('admin.events._fields')
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::button('Insert', ['class' => 'btn dark', 'type' => 'submit']) !!}
                                    {!! Form::button('Reset', ['class' => 'btn default', 'type' => 'reset', 'id' => 'reset']) !!}
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    <!-- END CONTENT-->
@endsection

@push('js')
    {!! Html::script('dateTimePicker/DateTimePicker.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $("#dtBox").DateTimePicker();
        });

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
@endpush