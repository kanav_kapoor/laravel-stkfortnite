@extends('admin.layouts.app')

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::style('metronic/global/plugins/datatables/datatables.min.css') !!}
    {!! Html::style('metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') !!}
    <!-- END PAGE LEVEL PLUGINS -->
@endpush


@section('content')
                   
    <h3 class="page-title"> Manage Events
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{ route('admin.home') }}">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li style="text-transform: capitalize;">
                calendar
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert" style="text-transform: capitalize;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('success') }}
        </div>
    @endif


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">list of events</span>
                    </div>
                    &nbsp;
                    <a href="{{ route('admin.events.create') }}" class="btn sbold red btn-outline"> Add New Event
                        <i class="fa fa-plus"></i>
                    </a>
                    
                </div>
                
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                            <tr>
                                <th class="col-md-1"> # </th>
                                <th class="col-md-2"> Title </th>
                                <th class="col-md-3"> Description </th>
                                <th class="col-md-2"> Start Date </th>
                                <th class="col-md-2"> End Date </th>
                                <th class="col-md-2"> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($events as $events)
                                <tr class="odd gradeX">
                                    <td> {{ $loop->iteration }} </td>
                                    <td>
                                        {{ $events->title }}
                                    </td>
                                    <td>
                                        {{ $events->description }}
                                    </td>
                                    <td class="center"> {{ $events->start_date }} </td>
                                    <td>
                                       {{ $events->end_date }}
                                    </td>
                                    <td>
                                        <a class="btn dark btn-outline btn-sm" href="{{ route('admin.events.edit', [$events->id]) }}"><span class="glyphicon glyphicon-cog"></span> Edit</a>

                                            <a class="btn red btn-outline btn-sm" href="events/{{ $events->id }}" data-method="delete" data-confirm="Are you sure?">
                                                <span class="glyphicon glyphicon-trash "></span> Delete
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

@push('js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::script('metronic/global/scripts/datatable.js') !!}
    {!! Html::script('metronic/global/plugins/datatables/datatables.min.js') !!}
    {!! Html::script('metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {!! Html::script('metronic/pages/scripts/table-datatables-managed.min.js') !!}
    <!-- END PAGE LEVEL SCRIPTS -->

    <script>
        window.csrfToken = '<?php echo csrf_token(); ?>';
    </script>

    {!! Html::script('js/deleteHandler.js') !!}
@endpush