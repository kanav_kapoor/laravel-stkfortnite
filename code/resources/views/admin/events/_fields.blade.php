  <div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label">
        {!! Form::text('title', null, ['class' => 'form-control', 'id' => 'form_control_1']) !!}
        {!! Form::label('form_control_1', 'Title') !!}
        <span class="help-block">Write some title</span>

    </div>
   
    <div class="form-group form-md-line-input form-md-floating-label">
        {!! Form::text('description', null, ['class' => 'form-control', 'id' => 'form_control_1']) !!}
        {!! Form::label('form_control_1', 'Description') !!}
        <span class="help-block">Write some description</span>

    </div>

    <div class="form-group form-md-line-input form-md-floating-label">
        {!! Form::dateTime('start_date', null, ['class' => 'form-control', 'id' => 'form_control_1']) !!}
        {!! Form::label('form_control_1', 'Event Start Date') !!}
        <span class="help-block">Give some start Date</span>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label">
        {!! Form::dateTime('end_date', null, ['class' => 'form-control', 'id' => 'form_control_1']) !!}
        {!! Form::label('form_control_1', 'Event End Date') !!}
        <span class="help-block">Give some end Date</span>
    </div>

	<div id="dtBox"></div>
 
</div>