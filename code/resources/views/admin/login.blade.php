@extends('admin.layouts.app')

{{-- OVER-RIDE CSS STARTS--}}
@section('portalCss')
    
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::style('metronic/global/plugins/select2/css/select2.min.css') !!}
    {!! Html::style('metronic/global/plugins/select2/css/select2-bootstrap.min.css') !!}
    <!-- END PAGE LEVEL PLUGINS -->


    <!-- BEGIN PAGE LEVEL STYLES -->
    {!! Html::style('metronic/pages/css/login.min.css') !!}
    <!-- END PAGE LEVEL STYLES -->
@endsection
{{-- OVER-RIDE CSS ENDS--}}


{{-- OVER-RIDE BODY --}}
@section('body')
    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="javascript:;" style="text-decoration: none;">
                <h1><span class="text-danger">stk</span><span class="text-primary">fortnite</span></h1> </a>
        </div>
        <!-- END LOGO -->
@endsection


@section('login')
    <!-- BEGIN LOGIN -->
    <div class="content">

        <!-- BEGIN LOGIN FORM -->
        {!! Form::open(['route' => 'admin.authenticate', 'class' => 'login-form']) !!}
            <h3 class="form-title font-green">Sign In</h3>
             @if (session('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('error') }}
                </div>
            @elseif (session('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('success') }}
                </div>
            @endif
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Enter your username and password. </span>
            </div>
            <div class="form-group">

                {!! Form::label('email', 'E-Mail Address', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
                {!! Form::email('email', session('email'), ['class' => 'form-control form-control-solid placeholder-no-fix' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Username', 'required']) !!}

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Password', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
                {!! Form::password('password', ['class' => 'form-control form-control-solid placeholder-no-fix' . ($errors->has('password') ? ' is-invalid' : ''), 'placeholder' => 'Password', 'required', 'autocomplete' => 'off']) !!}
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-actions">
                <label class="rememberme check mt-checkbox mt-checkbox-outline" style="float: left;">
                    <input type="checkbox" name="remember" value="1" />Remember Me
                    <span></span>
                </label>
                {!! Form::button('Login', ['class' => 'btn green uppercase', 'type' => 'submit', 'style' => 'float:right;margin-top:-3%;']) !!}
            </div>
            <a style="margin-right: 30%;" href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>

        {!! Form::close() !!}
        <!-- END LOGIN FORM -->

        <!-- BEGIN FORGOT PASSWORD FORM -->
        {!! Form::open(['class' => 'forget-form']) !!}
            <h3 class="font-green">Forget Password ?</h3>
            <p> Enter your e-mail address below to reset your password. </p>
            <div class="form-group">
                {!! Form::email('email', null, ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Email', 'autocomplete' => 'off']) !!}
            </div>
            <div class="form-actions">
                {!! Form::button('Back', ['class' => 'btn green btn-outline', 'id' => 'back-btn', 'type' => 'button']) !!}
                {!! Form::button('Submit', ['class' => 'btn btn-success uppercase pull-right', 'type' => 'submit']) !!}

            </div>
        {!! Form::close() !!}
        <!-- END FORGOT PASSWORD FORM -->
    </div>
    <div class="copyright"> 2018 © stkfortnite | Admin Panel</div>
@endsection

{{-- OVER-RIDE SCRIPTS WITH PARENT --}}
@section('pageLevelScripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::script('metronic/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
    {!! Html::script('metronic/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
    {!! Html::script('metronic/global/plugins/select2/js/select2.full.min.js') !!}
    <!-- END PAGE LEVEL PLUGINS -->


    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {!! Html::script('metronic/pages/scripts/login.min.js') !!}
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
