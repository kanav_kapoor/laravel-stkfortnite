@extends('layouts.app')

@section('content')
    <!-- main -->
    <section class="bg-image bg-image-sm" style="background-image: url('https://img.youtube.com/vi/BhTkoDVgF6s/maxresdefault.jpg');">
        <div class="overlay"></div>
        <div class="container">
            @yield('container')
        </div>
    </section>
@endsection