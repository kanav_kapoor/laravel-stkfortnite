<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <h4 class="footer-title">About Stkfortnite</h4>
                <p>STK recruits, awards, and supports some of the best Fortnite players in the game. If you make the roster we pay you to play and market on your behalf to get you the recognition, fans, and followers you deserve.</p>
                {{-- <p>Attached more then 60+ HTML pages and customized elements. Copy and paste your favourite section or build your own so easily.</p> --}}
            </div>
            <div class="col-sm-12 col-md-3">
                <h4 class="footer-title">Platform</h4>
                <div class="row">
                    <div class="col">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Members</a></li>
                            <li><a href="#">Calender</a></li>
                            <li><a href="#">News</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li><a href="games.html">Twitch</a></li>
                            <li><a href="reviews.html">Join The Team</a></li>
                            <li><a href="videos.html">Blog</a></li>
                            <li><a href="forums.html">Videos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <h4 class="footer-title">To Do</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, excepturi! Voluptatibus doloremque veritatis eligendi fuga inventore quibusdam ut corporis et iusto labore omnis fugiat neque ratione asperiores, dignissimos ab recusandae!</p>
                {{-- <div class="input-group m-t-25">
                    <input type="text" class="form-control" placeholder="Email">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Subscribe</button>
                    </span>
                </div> --}}
            </div>
        </div>
        <div class="footer-bottom">
            <div class="footer-social">
                <a href="#" target="_blank" data-toggle="tooltip" title="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" target="_blank" data-toggle="tooltip" title="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" target="_blank" data-toggle="tooltip" title="steam"><i class="fa fa-steam"></i></a>
                <a href="#" target="_blank" data-toggle="tooltip" title="twitch"><i class="fa fa-twitch"></i></a>
                <a href="#" target="_blank" data-toggle="tooltip" title="youtube"><i class="fa fa-youtube"></i></a>
            </div>
            <p>Copyright &copy; {{ date('Y') }} <a href="{{ route('home') }}">Stkfortnite</a>. All rights reserved.</p>
        </div>
    </div>
</footer>