@extends('layouts.app')

@section('content')
    <section class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">About Us</li>
            </ol>
        </div>
    </section>

    <section class="p-b-0">
        <div class="container">
            <div class="heading">
                <i class="fa fa-superpowers"></i>
                <h2>About Us</h2>
                {!! $about_us->content !!}
            </div>
        </div>
    </section>
@endsection