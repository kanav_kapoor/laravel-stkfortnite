@extends('layouts.app')

@section('content')
    <section class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Events</li>
            </ol>
        </div>
    </section>
    <section>
        <div class="container">
		    <div class="row">
		        <div class="col-md-8 col-md-offset-2">
		            <div class="panel panel-default">
		                <div class="panel-heading">List of events</div>

		                <div class="panel-body">
                            

		                </div>
		            </div>
		        </div>
		    </div>
        </div>
    </section>
@endsection