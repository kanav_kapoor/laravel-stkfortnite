@extends('layouts.app')

@section('content')
    <section class="bg-image" style="background-image: url('https://img.youtube.com/vi/zSd9McRXHZ8/maxresdefault.jpg');">
        <div class="overlay-light"></div>
        <div class="container">
            <a class="text-center" href="{{ route('members.show', $video->user->id) }}">Back To Videos</a>
            <div align="center" class="embed-responsive embed-responsive-16by9">
                <video autoplay controls class="embed-responsive-item">
                    <source src="{{ $video->url }}" type="video/mp4">
                </video>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="post post-single">
                        <div class="post-header">
                            <div>
                                <a href="{{ $video->user->profile_url }}"><img src="{{ $video->thumbnail_url }}" alt=""></a>
                            </div>
                            <div>
                                <h2 class="post-title">{{ $video->title }}</h2>
                                <div class="post-meta">
                                    <span><i class="fa fa-clock-o"></i> {{ $video->created_at_formatted }} by <a href="{{ $video->user->profile_url }}">Clark</a></span>
                                </div>
                            </div>
                        </div>
                        <p>{{ $video->description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection