@extends('layouts._background')

@section('container')
    <div class="row">
        <div class="col-12 col-sm-8 col-md-4 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="fa fa-video-camera"></i> Upload A New Video</h4>
                </div>
                <div class="card-block">
                    <div class="container">
                        {!! Form::open(['route' => 'videos.store', 'files' => true]) !!}
                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-pencil"></i>
                                {!! Form::text('title', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Video Title', 'maxlength' => 255, 'required' => 'required']) !!}
                            </div>

                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-pencil"></i>
                                {!! Form::textarea('description', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Video Description', 'rows' => 3]) !!}
                            </div>

                            <div class="form-group">
                                <label for="thumbnail_name">Video</label>
                                {!! Form::file('file_name', ['class' => 'form-control-file', 'accept' => 'video/*' , 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="thumbnail_name">Thumbnail</label>
                                {!! Form::file('thumbnail_name', ['class' => 'form-control-file', 'accept' => 'image/*' , 'required' => 'required']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('UPLOAD', ['class' => 'btn btn-dark btn-rounded btn-block']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection