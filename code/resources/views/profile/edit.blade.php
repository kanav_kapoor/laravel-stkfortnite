@extends('layouts._background')

@section('container')
    <div class="row">
        <div class="col-12 col-sm-8 col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="fa fa-user-plus"></i> Update Profile</h4>
                </div>
                <div class="card-block">
                    {!! Form::model($user, ['route' => 'profile.update', 'files' => 'true', 'method' => 'PATCH']) !!}
                        @include('auth._register-fields')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>$(function(){ $("[name='password'],[name='password_confirmation']").removeAttr('required') }); </script>
@endpush