@extends('layouts.app')

@push('css')
    
@endpush

@section('content')
    <section class="p-y-80">
        <div class="container">
            <div class="heading">
                <i class="fa fa-users"></i>
                <h2>Members</h2>
                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> --}}
            </div>
            <div class="row">
                @include('members._list', compact('members'))
                {{-- @foreach($members as $member)
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="card card-lg">
                            <div class="card-img">
                                <a href="{{ $member->profile_url }}"><img src="{{ $member->profile_photo_url }}" class="card-img-top" alt="Assassin's Creed Syndicate"></a>
                                <div class="badge badge-warning">pc</div>
                                <div class="card-likes">
                                    <a href="#">15</a>
                                </div>
                            </div>
                            <div class="card-block">
                                <h4 class="card-title"><a href="game-post.html">{{ $member->full_name }}</a></h4>
                                <div class="card-meta"><span>&nbsp;</span></div>
                                <p class="card-text">{{ str_limit($member->bio, 80, '...') }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach --}}
            </div>
        </div>
    </section>
    <!-- /main -->
@endsection