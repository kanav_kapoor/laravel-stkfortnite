@extends('layouts.app')

@push('css')
    <style> .invite-url { word-break: break-word;  } </style>
@endpush

@section('content')
    <section class="hero hero-profile" style="background-image: url('{{ $member->cover_photo_url }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="hero-block">
                <h5>{{ $member->full_name }}</h5>
                @if($viewingMyProfile)
                    <a class="btn btn-primary btn-sm btn-shadow btn-rounded btn-icon btn-add" href="{{ route('videos.create') }}" data-toggle="tooltip" title="Add Video" role="button"><i class="fa fa-video-camera"></i></a>
                @endif
            </div>
        </div>
    </section>

    <section class="toolbar toolbar-profile" data-fixed="true">
        <div class="container">
            <div class="profile-avatar">
                {{-- <a href="#"> --}}<img src="{{ $member->profile_photo_url }}" alt="">{{-- </a> --}}
                <div class="sticky">
                    <a href="#"><img src="img/user/avatar-sm.jpg" alt=""></a>
                    <div class="profile-info">
                        <h5>Nathan Drake</h5>
                        <span>@nathan</span>
                    </div>
                </div>
            </div>
            <div class="dropdown float-right hidden-md-down">
                {{-- <a class="btn btn-secondary btn-icon btn-sm m-l-25 float-right" href="#" data-toggle="dropdown" role="button"><i class="fa fa-cog"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item active" href="#">Setting</a>
                    <a class="dropdown-item" href="#">Mail</a>
                    <a class="dropdown-item" href="#">Report</a>
                    <a class="dropdown-item" href="#">Block</a>
                </div> --}}
            </div>
            <ul class="toolbar-nav hidden-md-down">
                <li class="active"><a href="#">Videos</a></li>
                {{-- <li><a href="#">About</a></li>
                <li><a href="#">Games (38)</a></li>
                <li><a href="#">Friends (628)</a></li>
                <li><a href="#">Images (23)</a></li>
                <li><a href="#">Videos</a></li>
                <li><a href="#">Groups</a></li>
                <li><a href="#">Forums</a></li> --}}
            </ul>
        </div>
    </section>

    <section class="p-y-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 hidden-md-down">
                    <!-- widget about -->
                    <div class="widget widget-about">
                        <h5 class="widget-title">
                            About Me
                             @if($viewingMyProfile)
                                <a class="text-primary" href="{{ route('profile.showEditForm') }}">(Edit)</a>
                            @endif
                            
                        </h5>
                        <p>{{ $member->bio }}</p>
                        <ul>
                            <li><i class="fa fa-clock-o"></i> Joined {{ $member->joining_month_year }}</li>
                            <li><i class="fa fa-map-marker"></i> {{ $member->country_name }}</li>
                            <li><a href="mailto:{{ $member->email }}"><i class="fa fa-envelope"></i> {{ $member->email }}</a></li>
                            @if($member->epic_username)
                                <li><i class="fa fa-user-secret"></i> {{ $member->epic_username }}</li>
                            @endif
                            @if($member->epic_platform_name)
                                <li><i class="fa fa-gamepad"></i> {{ $member->epic_platform_name }}</li>
                            @endif
                            <li>
                                <small><i class="fa fa-link"></i><a class="invite-url text-primary" href="{{ $member->invite_url }}" target="_blank">{{ $member->invite_url }}</a></small>
                            </li>
                        </ul>
                    </div>

                    @if($membersAcceptedCount = $member->usersAccepted->count())
                        <!-- widget friends -->
                        <div class="widget widget-friends">
                            <h5 class="widget-title">REFERRALS ACCEPTED <span>({{ $membersAcceptedCount }})</span></h5>
                            <ul>
                                @foreach($member->usersAccepted as $user)
                                    <li><a href="{{ $user->profile_url }}" data-toggle="tooltip" title="{{ $user->name }}"><img src="{{ $user->profile_photo_url }}" alt="{{ $user->name }}"></a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="col-lg-9">
                    <section>
                        <div class="row row-5">
                            @if(!$videos->count())
                                <div class="col-md-12 col-sm-6 col-md-3">
                                    <h4 class="text-center">No Videos Uploaded</h4>
                                </div>
                            @endif
                            @foreach($videos as $video)
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="card card-video">
                                        <div class="card-img">
                                            <a href="{{ route('videos.show', $video->id) }}">
                                            <img src="{{ $video->thumbnail_url }}" alt="{{ $video->title }}">
                                        </a>
                                            {{-- <div class="card-meta">
                                                <span>4:32</span>
                                            </div> --}}
                                        </div>
                                        <div class="card-block">
                                            <h4 class="card-title"><a href="{{ route('videos.show', $video->id) }}">{{ str_limit($video->title, 50, '...') }}</a></h4>
                                            <div class="card-meta">
                                                <span><i class="fa fa-clock-o"></i> {{ $video->created_at_formatted }}</span>
                                                <span>{{ $video->views }} views</span>
                                            </div>
                                            <p>{{ str_limit($video->description, 90, '...') }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    
                        @if($videos->count())
                            <div class="pagination-results m-t-10">
                                <span>Showing {{ $videos->firstItem() }} to {{ $videos->lastItem() }} of {{ $videos->total() }} videos</span>
                                <nav aria-label="Page navigation">
                                    {{ $videos->links() }}
                                </nav>
                            </div>
                        @endif
                    </section>
                </div>
            </div>
        </div>
    </section>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
