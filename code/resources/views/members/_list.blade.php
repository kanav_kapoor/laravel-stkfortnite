@foreach($members as $member)
    <div class="col-12 col-sm-6 col-md-4">
        <div class="card card-lg">
            <div class="card-img">
                <a href="{{ $member->profile_url }}"><img src="{{ $member->profile_photo_url }}" class="card-img-top" alt="Assassin's Creed Syndicate"></a>
            </div>
            <div class="card-block">
                <h4 class="card-title"><a href="{{ $member->profile_url }}">{{ $member->full_name }}</a></h4>
                <div class="card-meta"><span>{{-- June 13, 2017 --}}</span></div>
                <p class="card-text">{{ str_limit($member->bio, 80, '...') }}</p>
            </div>
        </div>
    </div>
@endforeach