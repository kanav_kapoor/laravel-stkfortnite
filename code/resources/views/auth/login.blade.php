@extends('layouts._background')

@section('container')
    <div class="row">
        <div class="col-12 col-sm-8 col-md-4 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="fa fa-user-plus"></i> Login to your account</h4>
                </div>
                <div class="card-block">
                    {!! Form::open(['route' => 'login']) !!}
                        <div class="form-group input-icon-left m-b-10">
                            <i class="fa fa-envelope"></i>
                            {!! Form::email('email', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Email Address', 'maxlength' => 100, 'required' => 'required']) !!}
                        </div>

                        <div class="form-group input-icon-left m-b-10">
                            <i class="fa fa-lock"></i>
                            {!! Form::password('password', ['class' => 'form-control form-control-secondary', 'placeholder' => 'Password', 'maxlength' => 100, 'required' => 'required']) !!}
                        </div>

                        <label class="custom-control custom-checkbox custom-checkbox-primary">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Remember me</span>
                        </label>
                        
                        <button type="submit" class="btn btn-primary m-t-10 btn-block">Login</button>
                    {!! Form::close() !!}

                    <div class="divider">
                        <span><a class="text-primary" href="{{ route('password.request') }}">Forgot your password?</a></span>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
