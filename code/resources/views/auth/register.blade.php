@extends('layouts._background')

@section('container')
    <div class="row">
        <div class="col-12 col-sm-8 col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="fa fa-user-plus"></i> Register a new account</h4>
                </div>
                <div class="card-block">
                    {!! Form::open(['url' => '/register', 'files' => 'true']) !!}
                        {!! Form::hidden('invite_code', $inviteCode) !!}
                        @include('auth._register-fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
