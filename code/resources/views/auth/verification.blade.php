@extends('layouts.homelayout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mdg-sbscptn">
            <div class="sbscrb-bx">
                <h3>{{ __('Verify Account') }}</h3>
                <div class="col-md-6 col-md-offset-3">
                    {!! Form::open(['route' => 'verification.verify']) !!}
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                {!! Form::email('email', null, ['class' => "form-control" . $errors->has('email') ? ' is-invalid' : '', 'required' => 'true', 'autofocus'=> 'true']) !!}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Email') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('register') }}">
                                    {{ __('Or Register Here') }}
                                </a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection