<div class="row">
    <div class="col-md-6">
        <div class="form-group input-icon-left m-b-10">
            <i class="fa fa-user"></i>
            {!! Form::text('first_name', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'First Name', 'maxlength' => 50, 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group input-icon-left m-b-10">
            <i class="fa fa-user"></i>
            {!! Form::text('last_name', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Last Name', 'maxlength' => 50, 'required' => 'required']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group input-icon-left m-b-10">
            <i class="fa fa-lock"></i>
            {!! Form::password('password', ['class' => 'form-control form-control-secondary', 'id' => 'password', 'placeholder' => 'Password', 'minlength' => 5, 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group input-icon-left m-b-10">
            <i class="fa fa-unlock"></i>
            {!! Form::password('password_confirmation', ['class' => 'form-control form-control-secondary', 'placeholder' => 'Repeat Password', 'equalTo' => '#password', 'required' => 'required']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group input-icon-left m-b-10">
            <i class="fa fa-envelope"></i>
            {!! Form::email('email', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Email Address', 'maxlength' => 100, 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
    {!! Form::select('country_code', $countries, 'US', ['class' => 'form-control form-control-secondary', 'placeholder' => 'Epic Username', 'placeholder' => '--Select Your Country--', 'required' => 'required']) !!}
</div>
    </div>
</div>
<div class="form-group input-icon-left m-b-10">
    <i class="fa fa-pencil"></i>
    {!! Form::textarea('bio', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Enter Your Bio.', 'maxlength' => 5000, 'rows' => 3]) !!}
</div>
<div class="divider"><span>Upload Photo</span></div>
<div class="form-group">
    <label for="profile_photo">Profile Photo</label>
    {!! Form::file('profile_photo', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
</div>
<div class="form-group">
    <label for="cover_photo">Cover Photo</label>
    {!! Form::file('cover_photo', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
</div>
<div class="divider"><span>EPIC Platform Details</span></div>
<div class="form-group input-icon-left m-b-10">
    <i class="fa fa-user-secret"></i>
    {!! Form::text('epic_username', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Epic Username', 'maxlength' => 100]) !!}
</div>
<div class="form-group">
    {!! Form::select('epic_platform', $epicPlatforms, null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Epic Username', 'placeholder' => '--What platform do you play on?--']) !!}
</div>
{{-- <div class="g-recaptcha-outer">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <div class="g-recaptcha" data-sitekey="6LeBwhwUAAAAAG1RDj-rS2Wu4WYNoV021q0z-LNY"></div>
</div>
<div class="divider"><span>Terms of Service</span></div>
<label class="custom-control custom-checkbox custom-checkbox-primary custom-checked">
    <input type="checkbox" class="custom-control-input" checked="">
    <span class="custom-control-indicator"></span>
    <span class="custom-control-description">Subscribe to monthly newsletter</span>
</label>
<label class="custom-control custom-checkbox custom-checkbox-primary">
    <input type="checkbox" class="custom-control-input">
    <span class="custom-control-indicator"></span>
    <span class="custom-control-description">Accept <a href="#" data-toggle="modal" data-target="#terms">terms of service</a></span>
</label> --}}
<button type="submit" class="btn btn-primary m-t-10 btn-block">Submit</button>