@extends('layouts._background')

@section('container')
    <div class="row">
        <div class="col-12 col-sm-8 col-md-4 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="fa fa-user-plus"></i> Reset Password</h4>
                </div>
                <div class="card-block">
                    {!! Form::open(['route' => 'password.request']) !!}
                        {{-- {!! Form::hidden('token', $token) !!} --}}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group input-icon-left m-b-10">
                            <i class="fa fa-user-secret"></i>
                            {!! Form::email('email', null, ['class' => 'form-control form-control-secondary', 'placeholder' => 'Email Address', 'maxlength' => 100, 'required' => 'required']) !!}
                        </div>

                        <div class="form-group input-icon-left m-b-10">
                            <i class="fa fa-lock"></i>
                            {!! Form::password('password', ['class' => 'form-control form-control-secondary', 'id' => 'password', 'placeholder' => 'Password', 'minlength' => 5, 'required' => 'required']) !!}
                        </div>

                        <div class="form-group input-icon-left m-b-10">
                            <i class="fa fa-unlock"></i>
                            {!! Form::password('password_confirmation', ['class' => 'form-control form-control-secondary', 'placeholder' => 'Repeat Password', 'equalTo' => '#password', 'required' => 'required']) !!}
                        </div>
                        
                        {!! Form::submit('Reset Password', ['class' => 'btn btn-primary m-t-10 btn-block']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
