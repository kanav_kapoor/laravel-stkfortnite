<?php

return [
    'location_user_uploads' => env('LOCATION_USER_UPLOADS'),
    'location_video_uploads' => env('LOCATION_VIDEO_UPLOADS'),
];