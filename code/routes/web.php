<?php

Auth::routes();
/*Route::get('/', function () {
    return view('home');
});*/

// =========================== MAIN SITE / FRONT END ROUTES ======================================

Route::get('/', 'HomeController@index')->name('home');

Route::get('about', 'HomeController@showAbout')->name('pages.about');

Route::get('profile', function(){
    return redirect()->route('members.show', auth()->id());
})->name('user.profile');

Route::get('verification', 'Auth\VerificationController@showVerificationForm')->name('verification.showVerificationForm');
Route::post('verification', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('verification/{activation_code}', 'Auth\VerificationController@activate')->name('verification.activate');

Route::get('refer/{invite_code}', function($inviteCode){
    return redirect()->route('register', ['invite_code' => $inviteCode]);
})->name('refer');

Route::group(['middleware' => ['auth']], function(){
    // Route::get('profile', 'ProfileController@show')->name('profile.show');
    Route::get('profile/edit', 'ProfileController@showEditForm')->name('profile.showEditForm');
    Route::patch('profile', 'ProfileController@update')->name('profile.update');
    Route::resource('members', 'MemberController');
    Route::resource('videos', 'VideoController');
});

Route::get('/events', 'EventController@index')->name('events');

Route::get('{post_identifier}', 'PostController@index')->where('post_identifier','blogs|news');
Route::get('{post_identifier}/{slug}', 'PostController@show')->where('post_identifier','blogs|news')->name('posts.show');




// ============================= ADMIN / BACK END ROUTES ========================================

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function(){

    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@authenticate')->name('authenticate');


    Route::group(['middleware' => 'auth.admin'], function(){

        Route::get('/', 'LoginController@checkAdminLogin')->name('home');
        Route::get('/about', 'StaticContentController@aboutUs')->name('about');
        Route::resource('members', 'MemberController');
        Route::post('about/{about}', 'StaticContentController@update')->name('about.update');

        // ======================== POSTS CRUD ===============================
        
        // VIEW POSTS........................
        Route::get('{post_identifier}', 'PostController@index')->where('post_identifier','blogs|news')->name('posts.index');
        
        // CREATE & STORE POSTS....................
        Route::get('{post_identifier}/create', 'PostController@create')->where('post_identifier','blogs|news')->name('posts.create');
        Route::post('{post_identifier}', 'PostController@store')->where('post_identifier','blogs|news')->name('posts.store');


        // EDIT & UPDATE POSTS..............
        Route::get('{post_identifier}/{post}/edit', 'PostController@edit')->where('post_identifier','blogs|news')->name('posts.edit');
        Route::patch('{post_identifier}/{post}', 'PostController@update')->name('posts.update');


        // DELETE POST...........
        Route::delete('{post_identifier}/{post}/delete', 'PostController@destroy')->name('posts.destroy');


        // ===================== EVENTS-CALENDAR CRUD ===============================
        
        // VIEW EVENTS........................
        Route::get('events', 'EventController@index')->name('events.index');

        // CREATE & STORE EVENTS....................
        Route::get('events/create', 'EventController@create')->name('events.create');
        Route::post('events', 'EventController@store')->name('events.store');


        // EDIT & UPDATE EVENT..............
        Route::get('events/{event}/edit', 'EventController@edit')->name('events.edit');
        Route::patch('{event}', 'EventController@update')->name('events.update');


        // DELETE EVENT...........
        Route::delete('events/{event}', 'EventController@destroy')->name('events.destroy');


        // LOGOUT.......
        Route::get('logout', 'LogoutController@logout')->name('logout');

    });

});