<?php

namespace App\Support\DataProviders;

class QualificationsProvider extends BaseProvider
{
    public static $data = [
        'high_school'   => 'High School',
        'undergraduate' => 'Undergraduate',
        'bachelors'     => 'Bachelors',
        'masters'       => 'Masters',
        'doctorate_phd' => 'Doctorate/PHD'
    ];
}