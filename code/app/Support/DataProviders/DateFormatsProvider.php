<?php

namespace App\Support\DataProviders;

class DateFormatsProvider extends BaseProvider
{
    public static $default = 'dd.mm.yyyy';

    public static $data = [
        'yyyy-mm-dd' => 'yyyy-mm-dd',
        'dd-mm-yyyy' => 'dd-mm-yyyy',
        'dd.mm.yyyy' => 'dd.mm.yyyy',
        'mm/dd/yyyy' => 'mm/dd/yyyy',
        'yyyy/mm/dd' => 'yyyy/mm/dd',
    ];
}