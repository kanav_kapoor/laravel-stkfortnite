<?php

namespace App\Support\DataProviders;

class DaysProvider extends BaseProvider
{
    protected static $default = 'mon';

    protected static $data = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
    ];
}