<?php

namespace App\Support\DataProviders;

class ExtensionProvider extends BaseProvider
{
    public static function all()
    {
        return [
            'Vector'   => ['eps', 'ai'],
            'Image'    => ['jpg', 'png', 'tif', 'psd'],
            'Document' => ['pdf', 'rtf', 'doc', 'docx', 'odt'],
            'Archive'  => ['gz', 'zip', 'rar', 'tar'],
        ];
    }
}