<?php

namespace App\Support\DataProviders;

class TimeFormatsProvider extends BaseProvider
{
    public static $default = 'hh.mm.ss';

    public static $data = [
        'hh.mm.ss' => 'hh.mm.ss',
        'hh:mm:ss' => 'hh:mm:ss',
        'hh-mm-ss' => 'hh-mm-ss',
        'hh/mm/ss' => 'hh/mm/ss',
    ];
}