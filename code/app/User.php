<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Support\DataProviders\CountriesProvider;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ADMIN_EMAIL = 'admin.stkfortnite@gmail.com';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rank_id',
        'first_name',
        'last_name',
        'email',
        'country_code',
        'password',
        'invite_code',
        'profile_photo',
        'cover_photo',
        'bio',
        'epic_username',
        'epic_platform',
        'is_featured',
        'activation_code',
        'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * List of Platforms
     */
    const EPIC_PLATFORMS = [
        'PS4',
        'xbox',
        'computer',
        'other'
    ];

    /**
     * Fire User Events
     * @var array
     */
    protected $dispatchesEvents = [
        'creating' => \App\Events\UserCreating::class,
    ];

    /**
     * The attributes that should be should be cast to native values
     *
     * @var array
     */
    protected $casts = [
        'is_active'    => 'boolean'
    ];

    /**
     * Active Users
     * @return string
     */
    public function scopeActive($query)
    {
        return $query->whereIsActive(true);
    }

    /**
     * Active Users
     * @return string
     */
    public function scopeExceptMe($query)
    {
        return $query->where('id', '!=', auth()->id());
    }

    /**
     * Full name of the user
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucwords(strtolower($this->first_name . ' ' . $this->last_name)) . " ({$this->rank->name})";
    }

    /**
     * Name of the country
     * @return string
     */
    public function getCountryNameAttribute()
    {
        return CountriesProvider::data()[$this->country_code];
    }

    /**
     * Joining month and year in string form
     * @return string
     */
    public function getJoiningMonthYearAttribute()
    {
        return date_format($this->created_at, 'F Y');
    }

    /**
     * Invite code url of the user
     * @return string
     */
    public function getInviteUrlAttribute()
    {
        return route('refer', $this->invite_code);
    }

    /**
     * Invite code url of the user
     * @return string
     */
    public function getProfileUrlAttribute()
    {
        return route('members.show', $this->id);
    }

    /**
     * Listing of Epic Platforms
     * @return array
     */
    public static function epicPlatforms()
    {
        $epicPlatforms = self::EPIC_PLATFORMS;
        return array_combine($epicPlatforms, array_map('ucwords', $epicPlatforms));
    }

    /**
     * Details of Epic platform
     * @return string
     */
    public function getEpicPlatformNameAttribute()
    {
        return ucwords($this->epic_platform);
    }

    /**
     * URL of Profile Photo
     * @return string
     */
    public function getProfilePhotoUrlAttribute()
    {
        return $this->profile_photo 
                ? asset(self::directoryLocation() . '/' . $this->profile_photo)
                : asset('imgs/placeholders/user-image.png');
    }

    /**
     * Directory path of Profile Photo
     * @return string
     */
    public function profilePhotoPath()
    {
        return public_path(self::directoryLocation() . DIRECTORY_SEPARATOR . $this->profile_photo);
    }

    /**
     * URL of Cover Photo
     * @return string
     */
    public function getCoverPhotoUrlAttribute()
    {
        return 
            $this->cover_photo ?
                asset(self::directoryLocation() . '/' . $this->cover_photo) :
                asset('imgs/placeholders/user-cover.png');
    }

    /**
     * Directory path of Cover Photo
     * @return string
     */
    public function coverPhotoPath()
    {
        return public_path(self::directoryLocation() . DIRECTORY_SEPARATOR . $this->cover_photo);
    }

    /**
     * Direction Locations For All User Uploads
     * @return string
     */
    public static function directoryLocation()
    {
        return config('custom.location_user_uploads');
    }

    /**
     * All Referrals sent by the user
     * @return \Illuminate\Support\Collection
     */
    public function referralsSent()
    {
        return $this->hasMany(Referral::class, 'referred_by', 'id');
    }

    /**
     * Videos uploaded by the user
     * @return \Illuminate\Support\Collection
     */
    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    /**
     * Accepted Referrals that were sent by the user
     * @return \Illuminate\Support\Collection
     */
    public function referralsAccepted()
    {
        return $this->referralsSent()->accepted();
    }
    
    /**
     * User who referred this user
     * @return \App\User
     */
    public function referredByReferral()
    {
        return $this->hasOne(Referral::class, 'referred_to', 'id');
    }

    /**
     * Users Referred
     * @return \Illuminate\Support\Collection
     */
    public function usersReferred()
    {
        return $this->belongsToMany(User::class, 'referrals', 'referred_by', 'referred_to');
    }

    /**
     * Users who accepted the referrals
     * @return \Illuminate\Support\Collection
     */
    public function usersAccepted()
    {
        return $this->usersReferred()->whereNotNull('referrals.accepted_at');
    }

    /**
     * Rank details of the user
     * @return App\Rank
     */
    public function rank()
    {
        return $this->belongsTo(Rank::class);
    }

    /**
     * Check if the user is active
     * @return boolean
     */
    public function isActive()
    {
        return $this->is_active == true;
    }

    /**
     * Check if the user is admin
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->email == self::ADMIN_EMAIL;
    }
}
