<?php

namespace App\Listeners\Auth;

use App\Events\UserCreating;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\Auth\UserRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAuthorisationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreating  $event
     * @return void
     */
    public function handle(UserCreating $event)
    {
        $event->user->notify(new UserRegistered($event->user));
    }
}
