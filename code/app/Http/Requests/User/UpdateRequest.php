<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::baseRules();
        
        $authId = auth()->id();
        
        $rules['password'] .= '|nullable';
        $rules['email'] .= ",email,{$authId}";
        $rules['epic_username'] .= ",epic_username,{$authId}";

        return $rules;
    }
}
