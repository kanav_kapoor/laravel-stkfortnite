<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Support\DataProviders\CountriesProvider;

abstract class AbstractRequest extends FormRequest
{
    protected static $rules = [
        'first_name'    => 'required|string|max:255',
        'last_name'     => 'required|string|max:255',
        'email'         => 'required|string|email|max:255|unique:users',
        'country_code'  => "required",
        'password'      => 'string|min:5|confirmed',
        'bio'           => 'nullable|max:5000',
        'epic_username' => 'nullable|string|max:255|unique:users',
        'epic_platform' => 'nullable|string|max:255',
        'profile_photo' => 'nullable|mimetypes:image/jpeg,image/png|mimes:jpeg,jpg,bmp,png',
        'cover_photo'   => 'nullable|mimetypes:image/jpeg,image/png|mimes:jpeg,jpg,bmp,png',
        'invite_code'   => 'nullable|exists:users'
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function baseRules()
    {
        $countries = CountriesProvider::data();
        
        $rules = self::$rules;

        $rules['country_code'] .= "|in:{$countries->keys()->implode(',')}";

        return $rules;
    }
}
