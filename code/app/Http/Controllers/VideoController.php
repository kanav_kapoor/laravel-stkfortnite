<?php

namespace App\Http\Controllers;

use App\Video;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VideoController extends Controller
{
    public function __construct()
    {
        return $this->middleware('video.views', ['only' => 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validateData($request);

        $user = auth()->user();

        try {
            $data['file_name'] = $this->moveVideoFile($data['file_name']);
            $data['thumbnail_name'] = $this->moveVideoFile($data['thumbnail_name']);
        } catch (Exception $e) {
            Log::error('Unable to move video files');
            return $this->failRedirect(route('videos.create'), 'Something went wrong. Try again Later');
        }

        $user->videos()->create($data);

        return $this->successRedirect(route('members.show', $user->id), 'Video Uploaded Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        // dd($video->url);
        return view('videos.show', compact('video'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validateData(Request $request)
    {
        return $request->validate([
            'title'          => 'required|max:255',
            'description'    => 'required|max:2000',
            'file_name'      => 'required|mimes:mp4,mov,ogg,qt,mp4,ogx,oga,ogv,ogg,webm,mpg,mpeg,avi|max:20000',
            'thumbnail_name' => 'required|mimetypes:image/jpeg,image/png|mimes:jpeg,jpg,bmp,png'
        ]);
    }

    /**
     * Move video files uploaded by user
     * @param  Illuminate\Support\Facades\File $file
     * @return Illuminate\Support\Collection
     */
    private function moveVideoFile($file)
    {
        return $this->moveOneFile(public_path(Video::directoryLocation()), $file)['name'];
    }
}
