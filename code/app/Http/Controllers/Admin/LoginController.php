<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\User;
use App\Event;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * Displays Login Form
     * @return Response
     */
    public function showLoginForm()
    {
    	return view('admin.login');
    }


    /**
     * Validates the input data
     * @param  Request $request 
     * @return object  
     */
    protected function validateCredentials($request)
    {
        $messages = [
            'required' => 'Please enter your :attribute.'
        ];
        $rules = [
        	'email' => 'required|email',
        	'password' => 'required'
        ];
        return $validator = Validator::make(['email' => $request->email, 'password' => $request->password], $rules, $messages);
    }


    /**
     * Authenticates by requesting authenticate method and then redirects to desired page
     * @param  Request $request 
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $validated = $this->validateCredentials($request);

        if ($validated->fails()) {
            return redirect()->back()->withErrors($validated)->withInput();

        } elseif (auth()->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return $this->successfullLogin();
        } else {
            return redirect()->back()->with('error', 'Username/Password Incorrect');
        }
    }

    public function checkAdminLogin()
    {
        if (auth()->check()) {

            return $this->successfullLogin();

        } else {
            return view('admin.login');
        }
    }



    /**
     * Successfully Logged In
     * @return Response
     */
    private function successfullLogin()
    {
        $user_count = User::count();
        $blogs_count = Post::whereType('blogs')->count();
        $news_count = Post::whereType('news')->count();
        $event_count = Event::count();
        
        return view('admin.home', compact('user_count','blogs_count','news_count','event_count'));
    }

}
