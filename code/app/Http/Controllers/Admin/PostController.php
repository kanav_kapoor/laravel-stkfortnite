<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($postIdentifier)
    {
        $posts = Post::whereType($postIdentifier)->get();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($postIdentifier)
    {
        return view('admin.posts.create', ['post_name' => $postIdentifier]);
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_identifier)
    {
         $request->validate([
            'heading' => 'required',
            'content' => 'required'
        ]);
         Post::create([
            'type' => $post_identifier,
            'heading' => $request->heading,
            'content' => $request->content
         ]);
        return redirect()->route('admin.posts.index',$post_identifier)->with('success',$post_identifier.' created successfully....');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminat$e\Http\Response.'
     */
    public function show($postIdentifier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($postIdentifier, Post $post)
    {
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $postIdentifier, Post $post)
    {
        $request->validate([
            'heading' => 'required',
            'content' => 'required',
        ]);
        $post->update($request->all());
        return redirect()->route('admin.posts.index', $postIdentifier)->with('success',$postIdentifier.' updated successfully...');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($postIdentifier, Post $post)
    {
        $post->delete();
        return redirect()->route('admin.posts.index', $postIdentifier)->with('success',$postIdentifier.' deleted successfully...');
    }
}
