<?php

namespace App\Http\Controllers\Admin;

use App\StaticContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticContentController extends Controller
{
    /**
     * Displays About Page
     * 
     * @return \Illuminate\Http\Response
     */
    public function aboutUs()
    {
        $about = StaticContent::where('slug','about')->first();
        return view('admin.about', compact('about'));
    }


    /**
     * Updates About Us Page
     * 
     * @param  Request         $request 
     * @param  StaticContent   $about     Model Binding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaticContent $about)
    {
        $request->validate([
            'content' => 'required'
        ]);
        $about->update($request->all());
        return redirect()->route('admin.about')->with('success', 'About page updated successfully...');
    }
}
