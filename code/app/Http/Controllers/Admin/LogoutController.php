<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {
    	auth()->logout();
    	return redirect()->route('admin.login')->with('success','Successfully Logout...');
    }
}
