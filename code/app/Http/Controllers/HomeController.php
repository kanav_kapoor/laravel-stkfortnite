<?php

namespace App\Http\Controllers;

use App\User;
use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class HomeController extends Controller
{
    const EVENT_BACKGROUND_COLOR = "#404240",
          URL = "index.php/events";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = User::active()->exceptMe()->take(10)->get();

        $events = [];
        $data = Event::all();
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new Carbon($value->start_date),
                    new Carbon($value->end_date),
                    $value->id,
                    // Add color,link and description on event
                 [
                     'color' => self::EVENT_BACKGROUND_COLOR,
                     'url' => self::URL,
                     'description' => $value->start_date->format('g:i A').' - '.$value->end_date->format('g:i A')
                 ]
                );
            }
        }
        $calendar = Calendar::addEvents($events)->setCallbacks([
            'eventRender' => 'function(event, element) {
                element.append(event.description);
            }'
        ]);

        return view('home', compact('members','calendar'));
    }


    /**
     * Displays About Page
     * 
     * @return \Illuminate\Http\Response
     */
    public function showAbout()
    {
        $about_us = DB::table('static_contents')->where('slug','about')->first();
        return view('pages.about', compact('about_us'));
    }
}
