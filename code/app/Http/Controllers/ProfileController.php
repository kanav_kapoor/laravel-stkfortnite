<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\User\UpdateRequest;
use App\Support\DataProviders\CountriesProvider;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('profile.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEditForm()
    {
        $user = $this->user();
        $epicPlatforms = User::epicPlatforms();
        $countries = CountriesProvider::data();

        return view('profile.edit', compact('user', 'epicPlatforms', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $data = $request->only('first_name', 'last_name', 'email', 'country_code', 'bio', 'epic_username', 'epic_platform');
        
        if(trim($request['password'])){
            $data['password'] = bcrypt($request->password);
        }

        if(isset($request['cover_photo'])){
            $data['cover_photo'] = $this->moveUserFile($request['cover_photo']);
        }

        if(isset($request['profile_photo'])){
            $data['profile_photo'] = $this->moveUserFile($request['profile_photo']);
        }

        $this->user()->update($data);

        return $this->successRedirect(route('profile.showEditForm'), 'Profile Updated Successfully');
    }

    /**
     * Instance of Authorised User
     * @return \App\User
     */
    public function user()
    {
        return auth()->user();
    }

    /**
     * Move photos uploaded by user
     * @param  Illuminate\Support\Facades\File $file
     * @return Illuminate\Support\Collection
     */
    private function moveUserFile($file)
    {
        return $this->moveOneFile(public_path(User::directoryLocation()), $file)['name'];
    }
}
