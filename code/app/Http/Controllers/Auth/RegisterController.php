<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Support\DataProviders\CountriesProvider;
use App\Http\Requests\User\AbstractRequest as AbstractUserRequest;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        $epicPlatforms = User::epicPlatforms();
        $countries = CountriesProvider::data();

        $inviteCode = $request->invite_code;

        return view('auth.register', compact('epicPlatforms', 'countries', 'inviteCode'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = AbstractUserRequest::baseRules();

        $rules['password'] .= '|required';
        
        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $coverPhoto = $profilePhoto = null;

        if(isset($data['profile_photo'])){
            $profilePhoto = $this->moveUserFile($data['profile_photo']);
        }

        if(isset($data['cover_photo'])){
            $coverPhoto = $this->moveUserFile($data['cover_photo']);
        }


        /*if(!$coverPhoto || !$profilePhoto){
            \Log::error('Unable to move photos');
        }*/

        $userData = [
            'first_name'    => $data['first_name'],
            'last_name'     => $data['last_name'],
            'email'         => $data['email'],
            'country_code'  => $data['country_code'],
            'password'      => Hash::make($data['password']),
            'bio'           => $data['bio'],
            'epic_username' => $data['epic_username'],
            'epic_platform' => $data['epic_platform'],
            'cover_photo'   => $coverPhoto,
            'profile_photo' => $profilePhoto
        ];
        
        $user = User::create($userData);

        if($inviteCode = $data['invite_code']){
            User::whereInviteCode($inviteCode)->first()->referralsSent()->create(['referred_to' => $user->id]);
        }

        return $user;
    }

    /**
     * Move photos uploaded by user
     * @param  Illuminate\Support\Facades\File $file
     * @return Illuminate\Support\Collection
     */
    private function moveUserFile($file)
    {
        return $this->moveOneFile(public_path(User::directoryLocation()), $file)['name'];
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {   
        auth()->logout();
        
        return $this->successRedirect(route('register'), 'Please click on the confirmation link sent on your email.');
    }

    /**
     * List of countries
     * @return Illuminate\Support\Collection
     */
    private function countries()
    {
        return CountriesProvider::data();
    }
}
