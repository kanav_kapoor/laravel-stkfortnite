<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\Auth\UserRegistered;

class VerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display form to reverify
     * @return \Illuminate\Http\Response
     */
    public function showVerificationForm()
    {
        return view('auth.verification');
    }

    public function verify(Request $request)
    {
        $emailValidationMessage = 'Account Not Found/Already Active';

        $data = $request->validate([
            'email' => 'required|email|exists:users,email,is_active,0'
        ], array_fill_keys(['email.required', 'email.exists'], $emailValidationMessage));

        $user = User::whereEmail($data['email'])->first();

        $user->notify(new UserRegistered($user));

        return $this->successRedirect(route('login'), 'Verification Email Resent Successfully');
    }

    /**
     * Activate account of user
     * @param  string $activationCode
     * @return \Illuminate\Http\Response
     */
    public function activate($activationCode)
    {
        $user = User::whereActivationCode($activationCode)->first();

        if(!$user){
            return $this->failRedirect(route('login'), 'Invalid Activation Code');
        }

        $user->update(['activation_code' => null, 'is_active' => true]);

        if($referredByReferral = $user->referredByReferral){
            $referredByReferral->update(['accepted_at' => Carbon::now()]);
        }

        auth()->login($user);
        
        return $this->successRedirect(route('home'), 'Your account has been activated.');

    }
}
