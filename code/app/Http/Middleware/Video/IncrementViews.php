<?php

namespace App\Http\Middleware\Video;

use Closure;

class IncrementViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->video->incrementView();
        return $next($request);
    }
}
