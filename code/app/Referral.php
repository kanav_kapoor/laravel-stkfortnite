<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'referred_by',
        'referred_to',
        'accepted_at'
    ];

    /**
     * Referrals that were are accepted
     * @param  \Illuminate\Support\Query $query
     * @return \Illuminate\Support\Query
     */
    public function scopeAccepted($query)
    {
        return $query->whereNotNull('accepted_at');
    }

    /**
     * User who sent the referral
     * @return \App\User
     */
    public function referredByUser()
    {
        return $this->belongsTo(User::class, 'referred_by', 'id');
    }
    
    /**
     * User who accepted the referral
     * @return \App\User
     */
    public function referredToUser()
    {
        return $this->belongsTo(User::class, 'referred_to', 'id');
    }
}
