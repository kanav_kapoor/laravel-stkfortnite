<?php

namespace App\ViewComposer;

use App\StaticContent;
use Illuminate\View\View;

class UserComposer {

    public function compose(View $view)
    {
        $view->with('authUser', auth()->user());
    }

}