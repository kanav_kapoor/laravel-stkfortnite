<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'file_name',
        'thumbnail_name',
        'duration_in_seconds',
        'title',
        'description',
        'views'
    ];

        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'views' => 0,
        'duration_in_seconds' => 0,
    ];

    /**
     * Videos uploaded by the user
     * @return \Illuminate\Support\Collection
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * URL of Video
     * @return string
     */
    public function getCreatedAtFormattedAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * URL of Video
     * @return string
     */
    public function getUrlAttribute()
    {
        return asset(self::directoryLocation() . '/' . $this->file_name);
    }

    /**
     * Directory path of video
     * @return string
     */
    public function videoPath()
    {
        return public_path(self::directoryLocation() . DIRECTORY_SEPARATOR . $this->file_name);
    }

    /**
     * URL of Thumbnail
     * @return string
     */
    public function getThumbnailUrlAttribute()
    {
        return asset(self::directoryLocation() . '/' . $this->thumbnail_name);
    }

    /**
     * Directory path of Thumbnail
     * @return string
     */
    public function thumbnailPath()
    {
        return public_path(self::directoryLocation() . DIRECTORY_SEPARATOR . $this->thumbnail_name);
    }

    /**
     * Direction Locations For All Video Uploads
     * @return string
     */
    public static function directoryLocation()
    {
        return config('custom.location_video_uploads');
    }

    /**
     * Increment a view of a video
     * @return null
     */
    public function incrementView()
    {
        $this->update(['views' => $this->views + 1]);
    }
}
