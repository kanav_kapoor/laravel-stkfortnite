<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'heading',
        'content',
        'cover_photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];


    /**
     * URL of cover photo
     * @return string
     */
    public function getCoverPhotoUrlAttribute()
    {
        return $this->cover_photo ?
                    asset(self::directory() . '/' .$this->type . '/'. $this->cover_photo) :
                    asset('imgs/placeholders/posts-default.jpg');
    }


    /**
     * Path of cover photo
     * @return string
     */
    public function getCoverPhotoPathAttribute()
    {
        return public_path(self::directory() . DIRECTORY_SEPARATOR .$this->type . DIRECTORY_SEPARATOR. $this->cover_photo);
    }


    /**
     * Directory Location for all posts
     * @return string
     */
    public static function directory()
    {
        return config('paths.post_files');
    }
}
