<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rank_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('country_code')->nullable();
            $table->string('password');
            $table->string('invite_code')->unique();
            $table->string('profile_photo')->nullable();
            $table->string('cover_photo')->nullable();
            $table->longText('bio')->nullable();
            $table->string('epic_username')->unique()->nullable();
            $table->string('epic_platform')->nullable();
            $table->boolean('is_featured')->nullable();
            $table->string('activation_code')->nullable();
            $table->boolean('is_active');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('rank_id')->references('id')->on('ranks')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
