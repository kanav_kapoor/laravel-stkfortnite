<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = [
            'recruit',
            'private',
            'sargent',
            'lieutenant',
            'captain',
            'general'
        ];

        $ranks = array_map(function($rank){
            return ['slug' => $rank, 'name' => ucwords($rank)];
        }, $ranks);

        
        DB::table('ranks')->insert($ranks);
    }
}
