<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    const ADMIN_RANK_ID = 6;
    const ADMIN_FIRST_NAME = "daniel";
    const ADMIN_LAST_NAME = "merchant";
    const ADMIN_EMAIL = "admin.stkfortnite@gmail.com";
    const ADMIN_IS_ACTIVE = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'rank_id' => self::ADMIN_RANK_ID,
        	'first_name' => self::ADMIN_FIRST_NAME,
        	'last_name' => self::ADMIN_LAST_NAME,
        	'email' => self::ADMIN_EMAIL,
        	'password' => bcrypt('secret'),
        	'invite_code' => str_random(30),
        	'is_active' => self::ADMIN_IS_ACTIVE
        ]);
    }
}